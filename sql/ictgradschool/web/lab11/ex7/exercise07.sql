-- Answers to Exercise 7 here
DROP TABLE IF EXISTS dbtest_ex07;

CREATE TABLE IF NOT EXISTS dbtest_ex07 (
  id         INT NOT NULL PRIMARY KEY,
  comment    TEXT,
  comment_id INT NOT NULL AUTO_INCREMENT,
  FOREIGN KEY (comment_id) REFERENCES dbtest_ex04 (id)
);

INSERT INTO dbtest_ex07 (id, comment)
VALUES (1, "It’s a nice confirmation when an author’s work is validated, and they can see the fruit of their labor."),
       (2, "The first thing I look for is personalization. This is so easy, all it takes is to just include the name of the author.");
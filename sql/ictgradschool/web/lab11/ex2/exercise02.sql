-- Answers to Exercise 2 here
DROP TABLE IF EXISTS dbtest_ex02;

CREATE TABLE IF NOT EXISTS dbtest_ex02 (
  username   VARCHAR(20),
  first_name CHAR(10),
  last_name  CHAR(10),
  email      VARCHAR(30)
);

INSERT INTO dbtest_ex02
VALUES ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com'),
       ('programmer2','Peter','Peterson','peter@microsoft.com'),
       ('programmer3','Pete','Peterson','pete@microsoft.com'),
       ('programmer4','Puppy','Peterson','puppy@microsoft.com')


-- Answers to Exercise 9 here
SELECT * FROM dbtest_ex03 ;
SELECT id, name, gender, year_born, joined FROM dbtest_ex03;
SELECT title FROM dbtest_ex04;
SELECT DISTINCT director FROM dbtest_ex06;
SELECT title FROM dbtest_ex06 WHERE weekly_rate <= 2;
SELECT name FROM dbtest_ex03 ORDER BY name;
SELECT username FROM dbtest_ex05 WHERE first_name LIKE 'Pete%';
SELECT username FROM dbtest_ex05 WHERE first_name LIKE 'Pete%' OR last_name LIKE 'Pete%';
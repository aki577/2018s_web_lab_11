-- Answers to Exercise 6 here
DROP TABLE IF EXISTS dbtest_ex06;

CREATE TABLE IF NOT EXISTS dbtest_ex06(
  id INT(2),
  title VARCHAR(50),
  director VARCHAR(20),
  weekly_rate INT(2),
  PRIMARY KEY (id),
  FOREIGN KEY (id) REFERENCES dbtest_ex03 (id)
);

INSERT INTO dbtest_ex06
VALUES (1, "Ready Player One", "Steven Spielberg", 2),
       (2, "Indiana Jones and the Kingdom of the Crystal Skull", "Steven Spielberg", 4),
       (3, "Lincoln", "Steven Spielberg",6);
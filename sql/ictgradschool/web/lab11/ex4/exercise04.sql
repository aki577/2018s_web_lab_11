-- Answers to Exercise 4 here
DROP TABLE IF EXISTS dbtest_ex04;

CREATE TABLE IF NOT EXISTS dbtest_ex04(
  id INT NOT NULL PRIMARY KEY ,
  title CHAR(10),
  content TEXT
);

INSERT INTO dbtest_ex04
VALUES (1,'balabala','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Diam sollicitudin tempor id eu. Lectus vestibulum mattis ullamcorper velit. Diam in arcu cursus euismod quis viverra nibh cras. Quam lacus suspendisse faucibus interdum. Diam phasellus vestibulum lorem sed risus ultricies tristique nulla. Mi tempus imperdiet nulla malesuada pellentesque elit eget gravida cum. Ultrices dui sapien eget mi proin. Ultricies mi eget mauris pharetra. Nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur. Arcu non sodales neque sodales. Suspendisse interdum consectetur libero id faucibus nisl tincidunt eget. Diam sit amet nisl suscipit adipiscing bibendum est ultricies. Leo in vitae turpis massa sed elementum tempus. Enim tortor at auctor urna nunc id cursus. Ultricies mi quis hendrerit dolor magna eget est.'),
       (2,'yeyyey','Et netus et malesuada fames ac turpis. Consequat interdum varius sit amet mattis vulputate enim. Quis varius quam quisque id diam vel quam. Purus non enim praesent elementum facilisis. Quisque egestas diam in arcu. Tellus mauris a diam maecenas sed enim ut. Mauris commodo quis imperdiet massa. Commodo ullamcorper a lacus vestibulum sed arcu non odio. Rhoncus dolor purus non enim. Adipiscing elit duis tristique sollicitudin. Sed augue lacus viverra vitae. Imperdiet proin fermentum leo vel. Dignissim suspendisse in est ante in nibh mauris. Massa sed elementum tempus egestas sed sed risus pretium quam. Diam quam nulla porttitor massa id neque. Porttitor leo a diam sollicitudin tempor. Eu consequat ac felis donec et odio. Fringilla urna porttitor rhoncus dolor purus non. Elementum integer enim neque volutpat ac tincidunt vitae semper.');
-- Answers to Exercise 8 here
DELETE FROM dbtest_ex05 WHERE username="programmer4";
ALTER TABLE dbtest_ex05
  DROP COLUMN email;
DROP TABLE dbtest_ex05;
UPDATE dbtest_ex06
SET title       = 'I changed this content! LOL',
    weekly_rate = 5
WHERE id = 1;
UPDATE dbtest_ex06
SET id = 5
WHERE title = "Lincoln";